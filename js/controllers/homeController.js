 /*
  Function: Deals for the UI of local user profile
  Interacts with DB: No
  Entry point: after login or redirect from admin page.

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */
'use strict';

app.controller('homeCtrl', ['$scope','userService', function($scope,userService){
	$scope.title='User';


	//fetch all from login service
	$scope.userInfo = {name: "Pulkit", type: "Authenticated user"};

	//seperate for some profile dependent view. can be resolved later on.
	$scope.userType = "Authenticated user";
	$scope.serverMessage = "Connected";
	$scope.session = "true";

	var root ="/app/partials/";
	var num_of_Menu = 3; //one extra but not implemented help menu
	$scope.menu = ['Dashboard', 'Help'];

	$scope.url= {header: root+"sidebar.html", footer: root+"footer.html",
	 homepage: root+"dashboard_icons.html"
	};

	var pages = [root+"profile.html", root+"help.html"];

	$scope.currentTab = pages[0];

	console.log("Initilizing active menu in adminCtrl");

	$scope.active = Array();
	$scope.active[0] = "active";
	$scope.active[1] = "Notactive";
	$scope.active[2] = "Notactive";

	$scope.changeView = function(menu_num){
		console.log("value of menu selected is "+ menu_num);
		console.log("before: "+ $scope.active[0]+ ", "+ $scope.active[1]);
		//tab is sign in or register
		for(var j = 0; j<num_of_Menu; j++){
			if(j==menu_num){
				
				$scope.active[j] = "active";

				$scope.currentTab = pages[j];
				//console.log("value is active now with index "+j);
			}
			else{
				$scope.active[j] = "Notactive";	
				
			}
			
		}
		console.log("After: "+ $scope.active[0]+ ", "+ $scope.active[1]);

		
	}

	$scope.logout=function(){
		userService.logout();
	}
}]);