 /*
  Function: Deals with the Admin profiles
  Interacts with DB: No
  Entry point: function calls

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

'use strict';

app.controller('adminCtrl', ['$scope','userService','$location', function ($scope,userService, $location) {
	$scope.title='Admin';
	
	

	var connected=userService.serverSessionStatus();
	connected.then(function (msg){
		if(msg.data!="admin") {
			console.log("You need to be admin to access this page");
			if(msg.data=="user") {
				$location.path('/home');
			}
			else{
				$location.path('/login');
			}		
		}
		else{ //Session for admin
				$scope.session = "true";
				//fetch all from login service
				$scope.userInfo = {name: "Pulkit", type: "Admin"};

				//seperate for some profile dependent view. can be resolved later on.
				$scope.userType = "Admin";
				$scope.serverMessage = "Connected";

				///////////////////////////Code for user Data///////////////////////////////////////////////
				
				$scope.data = "";
				var getAllData = function(){
					userService.getAllUser(function(msg){
			     		 $scope.data = msg;    
					});
				}


				$scope.changeUserProfile = function(id1, type1){
					//change the id into 

					console.log("value for the id"+ id1 + " is "+ type1);
					userService.updateType({id: id1, type: type1}, $scope);
					//call the php api and acording to reply serverMessage should be added.

				}
				////////////////////////////Menu related code/////////////////////////////////////////////////

				var root ="/app/partials/";
				var num_of_Menu = 3; //one extra but not implemented help menu
				$scope.menu = ['Dashboard', 'User Management', 'Help'];

				$scope.url= {header: root+"sidebar.html", footer: root+"footer.html",
				 homepage: root+"dashboard_icons.html"
				};

				var pages = [root+"profile.html", root+"userManagment.html", root+"help.html"];

				$scope.currentTab = pages[0];

				console.log("Initilizing active menu in adminCtrl");

				$scope.active = Array();
				$scope.active[0] = "active";
				$scope.active[1] = "Notactive";
				$scope.active[2] = "Notactive";

				$scope.changeView = function(menu_num){
					console.log("value of menu selected is "+ menu_num);
					console.log("before: "+ $scope.active[0]+ ", "+ $scope.active[1]);
					//tab is sign in or register
					for(var j = 0; j<num_of_Menu; j++){
						if(j==menu_num){
							
							$scope.active[j] = "active";

							$scope.currentTab = pages[j];
							if(j==1){
								getAllData();
							}
							//console.log("value is active now with index "+j);
						}
						else{
							$scope.active[j] = "Notactive";	
							
						}
						
					}
					console.log("After: "+ $scope.active[0]+ ", "+ $scope.active[1]);

				}
			$scope.logout=function(){
				userService.logout();
			}

		}

	});

}]);