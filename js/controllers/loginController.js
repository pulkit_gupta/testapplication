 /*
  Function: Manage all the login tasks
  Interacts with DB: No
  Entry point: ng-View

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */
'use strict';

app.controller('loginCtrl', ['$scope','userService', function ($scope,userService) {
	$scope.title='Login';

	$scope.session = "false";
	//seperate for some profile dependent view. can be resolved later on.
	$scope.userType = "Anonyous";
	$scope.serverMessage = "Ready for Login";

	var root ="/app/partials/";
	var num_of_Menu = 3; //one extra but not implemented help menu
	$scope.menu = ['Login', 'Register', 'Help'];

	$scope.url= {header: root+"sidebar.html", footer: root+"footer.html",
	 homepage: root+"dashboard_icons.html"
	};
	
	var pages = [root+"login.html", root+"register.html", root+"help.html"];

	$scope.currentTab = pages[0];

	console.log("Initilizing active menu");
	$scope.active = Array();
	$scope.active[0] = "active";
	$scope.active[1] = "Notactive";
	$scope.active[2] = "Notactive";

	$scope.changeView = function(menu_num){
		console.log("value of menu selected is "+ menu_num);
		console.log("before: "+ $scope.active[0]+ ", "+ $scope.active[1]);
		//tab is sign in or register
		for(var j = 0; j<num_of_Menu; j++){
			if(j==menu_num){
				
				$scope.active[j] = "active";

				$scope.currentTab = pages[j];
				//console.log("value is active now with index "+j);
			}
			else{
				$scope.active[j] = "Notactive";	
				
			}	
		}
		console.log("After: "+ $scope.active[0]+ ", "+ $scope.active[1]);
	}

	$scope.userPacket = {userName: "", password: "", repassword: "", email: ""};
	$scope.register = function(){
		console.log("values of USER NAME IS "+ $scope.userPacket.userName +" and password is "+ $scope.userPacket.password
		+" and re is "+ $scope.userPacket.repassword);

		//apply restriction and validations here
		userService.register($scope);
	}
	$scope.login=function(){
		$scope.serverMessage = "Trying to login";
		console.log("values of USER NAME IS "+ $scope.userPacket.userName +" and password is "+ $scope.userPacket.password);
		userService.login($scope.userPacket,$scope); //call login service
		
	};
}]);