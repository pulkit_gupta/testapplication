 /*
  Function: All the http post request are send from here. Talk with the server and in future TCP sockets 
  and zmq will also vbe used here. all the functional units are called from all the controllers 
  Interacts with DB: YES


  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

'use strict';
app.factory('userService',function($http, $location, sessionService){
	return{
		login:function(data1,$scope){
			console.log("sending post request");
			var $promise=$http.post('backend/login.php',data1); //send data to user.php
			$promise.then(function(msg){
				var uid=msg.data;
				console.log("result is "+msg.data);
				if(uid){
					//scope.msgtxt='Correct information';
					console.log("success");
					sessionService.set('uid',uid);
					$scope.serverMessage = "Success";
					$location.path('/admin');
				}	       
				else  {
					console.log("failed");
					$scope.userPacket.password = "";
					$scope.serverMessage = "Failed to login, please check your username or password.";

					//$location.path('/login');
				}				   
			});
		},
		logout:function(){
			sessionService.destroy('uid');
			$location.path('/login');
		},

		serverSessionStatus:function(){
			var session= $http.post('backend/session_check.php');
			return session;
		},

		getAllUser: function(callback){
			var checkSessionServer=$http.post('backend/getAllUser.php');
			checkSessionServer.then(function(msg){

				console.log("result is "+msg.data[0].name);
				callback(msg.data);
			});
		},

		islogged: function(){
			var checkSessionServer=$http.post('backend/member.php');
			checkSessionServer.then(function(msg){
				console.log("result is "+msg.data.status);

				if(msg.data.status=="admin"){
					var objJSON = angular.fromJson(msg.data.mainData);
					console.log("data  -> "+ objJSON[0].author);	
				}
				else{
					console.log("recieved is not admin");
				}
				

			});
			return checkSessionServer;
			/*
			if(sessionService.get('user')) return true;
			else return false;
			*/
		},

		register: function($scope){
			console.log("sending post request to register user "+ $scope.userPacket.userName);
			var $promise=$http.post('backend/register.php',$scope.userPacket); //send data to user.php
			$promise.then(function(msg){
				var uid=msg.data;
				console.log("result of registeration is "+msg.data);
				if(uid){
					//scope.msgtxt='Correct information';
					console.log("success");
					$scope.serverMessage = "User is successfully registered.";
					$scope.userPacket.userName = "";
					$scope.userPacket.email = "";
					$scope.userPacket.password = "";
					$location.path('/login');
				}	       
				else  {
					console.log("failed to register");
					$scope.userPacket.userName = "";
					$scope.userPacket.email = "";
					$scope.userPacket.password = "";
					$scope.serverMessage = "Failed to register, please try changing your username.";
				}				   
			});
		},

		updateType: function(data1, $scope){
			console.log("Updating the user with id "+ data1.id+" and type= "+data1.type);
			var $promise=$http.post('backend/updateUser.php',data1); //send data to user.php
			$promise.then(function(msg){
				var value=msg.data;
				console.log("result of update is "+msg.data);
				if(value){
					//scope.msgtxt='Correct information';
					console.log("success in update");
					$scope.serverMessage = "User with id: "+data1.id+" is now having role: "+data1.type;
				}	       
				else  {
					console.log("failed to update");
					$scope.serverMessage = "Failed to update. Something went wrong!";
				}				   
			});
		}

	}

});