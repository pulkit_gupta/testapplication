 /*
  Function: Deals with the web browser data for sessions mainly.
  Interacts with DB: No
 

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

'use strict';

app.factory('sessionService', ['$http', function($http){
	return{
		set:function(key,value){
			return sessionStorage.setItem(key,value);
		},
		get:function(key){
			return sessionStorage.getItem(key);
		},
		destroy:function(key){
			$http.post('backend/session_destroy.php');
			return sessionStorage.removeItem(key);
		}
	};
}])