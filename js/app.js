 /*
  Function: Main AngularJS app instialization
  Interacts with DB: No

  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */


'use strict';
// Declare app level module which depends on filters, and services
var app= angular.module('myApp', ['ngRoute']);
app.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {templateUrl: 'partials/anonymous.html', controller: 'loginCtrl'});

  $routeProvider.when('/home', {templateUrl: 'partials/home.html', controller: 'homeCtrl'});

  $routeProvider.when('/admin', {templateUrl: 'partials/admin.html', controller: 'adminCtrl'});

  $routeProvider.otherwise({redirectTo: '/login'});
}]);


//we will do it manually in each controller untill a proper rule is formed.


/*
app.run(function($rootScope, $location, loginService){
	

	var routespermission=['/home', 'admin'];  //route that require login
	$rootScope.$on('$routeChangeStart', function($http){
		if( routespermission.indexOf($location.path()) !=-1)
		{
			var connected=loginService.serverSessionStatus();
			connected.then(function(msg){
				console.log("status for server session is "+ msg.data);
				if(msg.data=="anonymous") {
					console.log("redirected because there is no session from server query.");
					$location.path('/login');
				}

			});
		}
	});
});*/



