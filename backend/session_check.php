<?php
	
	/*
	  Function: API to send the user profile type for client side authentication from session glocbal variable.
	  Interacts with DB: No
	

	  Author: Pulkit Gupta
	  Query email: pulkit.itp@gmail.com
	 */

	include 'ChromePhp.php';
	//	ChromePhp::log('Hello console!');

	session_start();	
	if( isset($_SESSION['uid'])){
		print $_SESSION['type'];   //gives either user or admin
	}
	else{
		ChromePhp::log('Server: I am sending the session status as anonymous');
		print 'anonymous';
	}
?>