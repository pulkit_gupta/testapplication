<?php 
	
	/*
  Function: TEST PAGE ONLY
  Interacts with DB: Yes


  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */
	
	include 'ChromePhp.php';
	//	ChromePhp::log('Hello console!');

	session_start();	

	if( isset($_SESSION['uid'])){
		

			//send the admin page to be reditrected with admin data

			 $json = array(
			 	"status" => "admin",
			     "mainData" => array(
			     array(
			         "title" => "Professional JavaScript",
			         "author" => "Nicholas C. Zakas"
			     ),
			     array(
			         "title" => "JavaScript: The Definitive Guide",
			         "author" => "David Flanagan"
			     ),
			     array(
			         "title" => "High Performance JavaScript",
			         "author" => "Nicholas C. Zakas"
			     ))

			 );

   			
			 $jsonstring = json_encode($json);

			 ChromePhp::log("SERVER: Sent JSON is ".$jsonstring);

 			 print $jsonstring;
			
			//print $_SESSION['type'];	//admin data
	
	} 

	else {

		$json = array(
			 	"status" => "anonymous"
			 );

			 $jsonstring = json_encode($json);
 			 print $jsonstring;
	}
?>