<?php
/*
  Function: API to response with the all user JSON array
  Interacts with DB: Yes


  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */

	session_start();	
	if(  !isset($_SESSION['uid']) || $_SESSION['type'] !== "admin"){
		
		print "type who is requesting is ".$_SESSION['type'];
		
		print " _____ Hence error";  //normal users and unregistered users can not access this API*/
		
	}
	else{
		
		include 'Db.php';

		// Create connection Singlton class
		$db = new Db();

		$sql = "SELECT id, name, email, type, timestmp FROM user WHERE 1";

		$result = $db -> select($sql);
		
		//print $result;

		print json_encode($result);
	}

?>