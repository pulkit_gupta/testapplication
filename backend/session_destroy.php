<?php 
	/*
	  Function: Destroy the session
	  Interacts with DB: No


	  Author: Pulkit Gupta
	  Query email: pulkit.itp@gmail.com
	 */
	session_id('uid');
	session_start();
	session_destroy();
	session_commit();
?>