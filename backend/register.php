<?php


/*
  Function: API to register the user sent in form of JSON and send response if everything is done.
  Interacts with DB: Yes


  Author: Pulkit Gupta
  Query email: pulkit.itp@gmail.com
 */
include 'Db.php';

$user=json_decode(file_get_contents('php://input'));  //get user from


// Create connection Singlton class
$db = new Db();

//get all variables from post here

//$_POST['username'];

$namePost = $user->userName;
$emailPost = $user->email;
$passwordPost = $user->password;
$typePost = "user";

$passwordval = md5($passwordPost);


/*

//secure quoting to avoid sql injection

$name = $db.quote($namePost);
$email = $db.quote($emailPost);
$password = $db.quote($passwordval);
$type = $db.quote($typePost);

$date = $db.quote($datePost);
*/
$date = date('Y-m-d H:i:s');
//echo $date;


/*$sql = "INSERT INTO user (name, email, password, type, timestmp) VALUES ( '".$name."' , '".$email."' , '".$password."' , '".$type."' , '".$date."' )";*/

//$sql = "INSERT INTO user (name, email, password, type, timestmp) VALUES ('".$namePost."','john@example.com', '".$passwordval."' , 'admin', '".$date."' )";
$sql = "INSERT INTO user (name, email, password, type, timestmp) VALUES ('".$namePost."','".$emailPost."', '".$passwordval."' , 'user', '".$date."' )";

    
$result = $db -> query($sql);

if($result == 1){
	echo 'ok';
	 }
else{
	echo "fail";
}


?>