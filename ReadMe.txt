
This is a test web application, in which there are three profiles of registered users namely anonyous, user and admin. The new user registered are normal users. Only admin login can make any user equivalent to admin rights. Normal user can only see his own profile.

First Admin details: 

username: admin
password: pulkit1


To run the application, please follow these steps:

1. Install Nginx or apache with PHP and mysql configuration.
2. Download the source code and put the app folder in the www folder of the nginx or apache.
3. Add the database with the following schema:

	id - autoincrement  PK
	name - text
	email - text
	password - text  (md5)
	type - text
	timestmp - text (Data time format)

4. Open file app/backend/config.ini and save the user, database name and password to login in mysql.

5. Start the web server.

6. To debug, open the console of browser, all the data is being displayed on it currently.

7. chromephp.php is a 3rd party library used to see the debug message of php on the client side.


Structure of code:

App folder has all the public file and a backend folder which contain all the php files and these files are mainly API POST response for the front end.

For the front end, although for such small projct, plain js could be used, but ANgular is much better as it gives a proper structure to the app and hence easy to expand app in future.


This application is made in very fast pace, so there can be small bugs.
For more details, Please contact me on the details below.

Programer: Pulkit Gupta
Email: pulkit.itp@gmail.com
